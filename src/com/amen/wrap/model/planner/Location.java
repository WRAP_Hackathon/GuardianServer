package com.amen.wrap.model.planner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.json.JSONObject;

@Entity
@Table(name = "location", uniqueConstraints = { @UniqueConstraint(columnNames = "location_id") })
public class Location implements IJSONParsable {

	private int location_id;
	private String name;
	private double latitude;
	private double longitude;

	public Location() {
	}

	public Location(double latitude, double longitude) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Location(int location_id, String name, double latitude, double longitude) {
		super();
		this.location_id = location_id;
		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Id
	@GeneratedValue
	@Column(name = "location_id")
	public int getLocation_id() {
		return location_id;
	}

	public void setLocation_id(int location_id) {
		this.location_id = location_id;
	}

	@Column(name = "name", nullable = true, length = 255)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "latitude", nullable = false)
	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	@Column(name = "longitude", nullable = false)
	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();

		obj.put("name", name);
		obj.put("latitude", latitude);
		obj.put("longitude", longitude);

		return obj;
	}
}
