package com.amen.wrap.model.planner;

import java.time.LocalDate;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.IndexColumn;
import org.json.JSONArray;
import org.json.JSONObject;

@Entity
@Table(name = "schedule", uniqueConstraints = { @UniqueConstraint(columnNames = "schedule_id") })
public class Schedule implements IJSONParsable {

	private int schedule_id;
	private LocalDate date;

	private List<SimpleTask> tasks;

	public Schedule() {
		tasks = new LinkedList<>();
	}

	@Id
	@GeneratedValue
	@Column(name = "schedule_id")
	public int getSchedule_id() {
		return schedule_id;
	}

	public void setSchedule_id(int schedule_id) {
		this.schedule_id = schedule_id;
	}

	@Column(name = "date", nullable = false)
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "schedule", cascade = CascadeType.ALL)
	public List<SimpleTask> getTasks() {
		return tasks;
	}

	public void setTasks(List<SimpleTask> tasks) {
		this.tasks = tasks;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		obj.put("schedule_id", schedule_id);
		obj.put("date", date.toString());
		
		JSONArray arrayOfTasks = new JSONArray();
		for(SimpleTask task : tasks){
			arrayOfTasks.put(task.toJSON());
		}
		obj.put("tasks", arrayOfTasks);
		
		return obj;
	}
}
