package com.amen.wrap.model.planner;

import java.time.LocalTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.json.JSONObject;

@Entity
@Table(name = "task", uniqueConstraints = { @UniqueConstraint(columnNames = "task_id") })
public class SimpleTask implements IJSONParsable{

	protected int task_id;
	protected String name;
	protected LocalTime time;
	protected Location location;
	
	protected Schedule schedule;

	public SimpleTask() {
		super();
	}

	public SimpleTask(int id, String name, LocalTime time) {
		super();
		this.task_id = id;
		this.name = name;
		this.time = time;
	}

	@Id
	@GeneratedValue
	@Column(name = "task_id")
	public int getId() {
		return task_id;
	}

	public void setId(int id) {
		this.task_id = id;
	}

	@Column(name = "name", nullable = false, length = 255)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "time", nullable = false)
	public LocalTime getTime() {
		return time;
	}

	public void setTime(LocalTime time) {
		this.time = time;
	}

	@OneToOne(cascade = CascadeType.ALL)
	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@ManyToOne
    @JoinColumn(name = "schedule_id")
	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	
	@Override
	public JSONObject toJSON() {
		JSONObject obj = new JSONObject();
		obj.put("task_id", task_id);
		obj.put("name",name);
		obj.put("time", time);
		obj.put("location", location.toJSON());
		
		return obj;
	}

}
