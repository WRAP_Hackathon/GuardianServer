package com.amen.wrap.model.planner;

import org.json.JSONObject;

public interface IJSONParsable {
	public JSONObject toJSON();

}
