package com.amen.wrap.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.json.JSONObject;

import com.amen.wrap.model.planner.IJSONParsable;

@Entity
@Table(name = "notification", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class Notification implements IJSONParsable {
	@Id
	@Column
	@GeneratedValue
	private int id;
	
	@Column
	private Date time;
	
	@Column
	private String message;
	
	@Column
	private boolean toGuardian;
	
	@Column 
	private boolean seen;
	

	public Notification() {
	}

	public Notification(int id, Date time, String message, boolean toGuardian, boolean seen) {
		super();
		this.id = id;
		this.time = time;
		this.message = message;
		this.toGuardian = toGuardian;
		this.seen = seen;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isToGuardian() {
		return toGuardian;
	}

	public void setToGuardian(boolean toGuardian) {
		this.toGuardian = toGuardian;
	}

	public boolean isSeen() {
		return seen;
	}

	public void setSeen(boolean seen) {
		this.seen = seen;
	}
	
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject o = new JSONObject();
		
		o.put("id", id);
		o.put("msg", message);
		o.put("toGuardian", toGuardian);
		o.put("seen", seen);
		o.put("date", new SimpleDateFormat("yyyy/MM/dd HH:mm").format(time));
		
		return o;
	}
}
