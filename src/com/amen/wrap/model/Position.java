package com.amen.wrap.model;


import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.json.JSONObject;

import com.amen.wrap.model.planner.IJSONParsable;

@Entity
@Table(name = "position", uniqueConstraints = { @UniqueConstraint(columnNames = "id") })
public class Position implements IJSONParsable {
	@Id
	@GeneratedValue
	@Column
	private int id;

	@Column 
	private Date time;
	
	@Column
	private double lat;

	@Column
	private double lon;

	public Position() {
		// TODO Auto-generated constructor stub
	}
	
	public Position(int id, Date time, double lat, double lon) {
		super();
		this.id = id;
		this.time = time;
		this.lat = lat;
		this.lon = lon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}	
	
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Override
	public JSONObject toJSON() {
		JSONObject o = new JSONObject();
		
		o.put("id", id);
		o.put("date", new SimpleDateFormat("yyyy/MM/dd HH:mm").format(time));
		o.put("lat", lat);
		o.put("lon", lon);
		
		return o;
	}
}
