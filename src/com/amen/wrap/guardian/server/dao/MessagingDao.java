package com.amen.wrap.guardian.server.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.slf4j.LoggerFactory;

import com.amen.wrap.guardian.server.HibernateSession;
import com.amen.wrap.guardian.server.servlet.RequestResult;
import com.amen.wrap.guardian.server.servlet.Result;
import com.amen.wrap.model.Notification;
import com.amen.wrap.model.Position;
import com.amen.wrap.model.planner.Schedule;

public class MessagingDao {
	private static MessagingDao instance = new MessagingDao();

	private MessagingDao() {

	}

	public static MessagingDao getInstance() {
		return instance;
	}

	public Result addPosition(Position position) {
		Result result = new Result();

		Session session = HibernateSession.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.save(position);

			session.getTransaction().commit();
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + e.getMessage());
			result.setResult(RequestResult.RESULT_FAIL);
			result.setMessage(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;
	}

	public Result getPositions() {
		Result result = new Result();

		Session session = HibernateSession.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Position.class);
			List<Position> returnList = (List<Position>) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.list();

			result.setReturnable(returnList);

			session.getTransaction().commit();
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + e.getMessage());
			result.setResult(RequestResult.RESULT_FAIL);
			result.setMessage(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;
	}
	
	public Result addMessage(Notification m){
		Result result = new Result();

		Session session = HibernateSession.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.save(m);

			session.getTransaction().commit();
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + e.getMessage());
			result.setResult(RequestResult.RESULT_FAIL);
			result.setMessage(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;
	}
	

	public Result getMessages() {
		Result result = new Result();

		Session session = HibernateSession.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Notification.class);
			List<Notification> returnList = (List<Notification>) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
					.list();

			result.setReturnable(returnList);

			session.getTransaction().commit();
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + e.getMessage());
			result.setResult(RequestResult.RESULT_FAIL);
			result.setMessage(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;
	}
}
