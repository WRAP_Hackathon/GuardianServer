package com.amen.wrap.guardian.server.dao;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.LoggerFactory;

import com.amen.wrap.guardian.server.HibernateSession;
import com.amen.wrap.guardian.server.servlet.RequestResult;
import com.amen.wrap.guardian.server.servlet.Result;
import com.amen.wrap.model.planner.Schedule;

public class ScheduleDao {
	private static ScheduleDao instance = new ScheduleDao();

	private ScheduleDao() {

	}

	public static ScheduleDao getInstance() {
		return instance;
	}

	public Result addSchedule(Schedule schedule) {
		Result result = new Result();

		Session session = HibernateSession.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			session.save(schedule);

			session.getTransaction().commit();
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + e.getMessage());
			result.setResult(RequestResult.RESULT_FAIL);
			result.setMessage(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;
	}
	
	public Result getScheduleFor(String day){
		Result result = new Result();

		Session session = HibernateSession.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Schedule.class);
			List<Schedule> returnList = (List<Schedule>) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
			for(Schedule schedule : returnList){
//				if(schedule.getDate().getDa.isEqual(LocalDate.now())){
					result.setReturnable(schedule);
//					break;
//				}
			}

			session.getTransaction().commit();
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + e.getMessage());
			result.setResult(RequestResult.RESULT_FAIL);
			result.setMessage(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;
	}

	public Result getSchedule(int id) {
		Result result = new Result();

		Session session = HibernateSession.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Schedule.class);
			Schedule returnInstance = (Schedule) criteria.add(Restrictions.eq("schedule_id", id)).uniqueResult();

			result.setReturnable(returnInstance);

			session.getTransaction().commit();
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + e.getMessage());
			result.setResult(RequestResult.RESULT_FAIL);
			result.setMessage(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;
	}

	public Result getSchedules() {
		Result result = new Result();

		Session session = HibernateSession.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			Criteria criteria = session.createCriteria(Schedule.class);
			List<Schedule> returnList = (List<Schedule>) criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

			result.setReturnable(returnList);

			session.getTransaction().commit();
		} catch (Exception e) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + e.getMessage());
			result.setResult(RequestResult.RESULT_FAIL);
			result.setMessage(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return result;
	}
}
