package com.amen.wrap.guardian.server.servlet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;

import com.amen.wrap.guardian.server.HibernateSession;
import com.amen.wrap.guardian.server.dao.MessagingDao;
import com.amen.wrap.guardian.server.dao.ScheduleDao;
import com.amen.wrap.model.Notification;
import com.amen.wrap.model.Position;
import com.amen.wrap.model.planner.IJSONParsable;
import com.amen.wrap.model.planner.Location;
import com.amen.wrap.model.planner.Schedule;
import com.amen.wrap.model.planner.SimpleTask;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/message")
public class MessagingServlet {

	@POST
	@Path("/sendMessage")
	@Produces("application/json")
	@Consumes("application/json")
	public Response sendMessage(String data) {
		JSONObject message = new JSONObject(data);
		Result r = new Result();

		try {
			Notification n = new Notification();
			n.setMessage(message.getString("msg"));
			n.setSeen(message.getBoolean("seen"));
			Date d = new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(message.getString("date"));
			n.setTime(d);
			n.setToGuardian(message.getBoolean("toGuardian"));

			MessagingDao.getInstance().addMessage(n);
		} catch (ParseException pe) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + pe.getMessage());
			r.setResult(RequestResult.RESULT_FAIL);
			r.setMessage(pe.getMessage());
		}

		return Response.status(200).entity(composeResult(r).toString()).build();
	}

	@POST
	@Path("/getMessages")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getMessages(String data) {
		Result messages = MessagingDao.getInstance().getMessages();

		return Response.status(200).entity(composeResult(messages).toString()).build();
	}

	@POST
	@Path("/getPositions")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getPositions(String data) {
		Result positions = MessagingDao.getInstance().getPositions();

		return Response.status(200).entity(composeResult(positions).toString()).build();
	}

	@POST
	@Path("/addPosition")
	@Consumes("application/json")
	@Produces("application/json")
	public Response addPosition(String data) {
		JSONObject scheduleObj = new JSONObject(data);
		Result r = new Result();

		try {
			Date d = new SimpleDateFormat("yyyy/MM/dd HH:mm").parse(scheduleObj.getString("date"));
			Double lat = scheduleObj.getDouble("lat");
			Double lon = scheduleObj.getDouble("lon");

			Position p = new Position();
			p.setLat(lat);
			p.setLon(lon);
			p.setTime(d);

			r = MessagingDao.getInstance().addPosition(p);
		} catch (ParseException pe) {
			LoggerFactory.getLogger(getClass()).error("Exception: " + pe.getMessage());
			r.setResult(RequestResult.RESULT_FAIL);
			r.setMessage(pe.getMessage());
		}

		return Response.status(200).entity(composeResult(r).toString()).build();
	}

	private JSONObject composeResult(Result resultToCompose) {
		JSONObject resultObject = new JSONObject();

		resultObject.put("result", resultToCompose.getResult().toString());
		if (resultToCompose.getReturnable() instanceof List) {
			List<IJSONParsable> parsables = (List<IJSONParsable>) resultToCompose.getReturnable();

			JSONArray array = new JSONArray();
			for (IJSONParsable parsable : parsables) {
				array.put(parsable.toJSON());
			}

			resultObject.put("instance", array);
		} else if (resultToCompose.getReturnable() instanceof IJSONParsable) {
			IJSONParsable parsable = (IJSONParsable) resultToCompose.getReturnable();
			resultObject.put("returnable", parsable.toJSON());
		}

		return resultObject;
	}

}
