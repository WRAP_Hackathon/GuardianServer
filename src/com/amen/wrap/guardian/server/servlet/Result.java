package com.amen.wrap.guardian.server.servlet;

public class Result {
	private RequestResult result;
	private String message;
	private Object returnable;

	public Result() {
		super();
		result = RequestResult.RESULT_OK;
	}

	public Result(RequestResult result, String message) {
		super();
		this.result = result;
		this.message = message;
	}

	public RequestResult getResult() {
		return result;
	}

	public void setResult(RequestResult result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getReturnable() {
		return returnable;
	}

	public void setReturnable(Object returnable) {
		this.returnable = returnable;
	}

}
