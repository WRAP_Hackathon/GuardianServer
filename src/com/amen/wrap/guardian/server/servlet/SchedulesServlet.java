package com.amen.wrap.guardian.server.servlet;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import com.amen.wrap.guardian.server.HibernateSession;
import com.amen.wrap.guardian.server.dao.ScheduleDao;
import com.amen.wrap.model.planner.IJSONParsable;
import com.amen.wrap.model.planner.Location;
import com.amen.wrap.model.planner.Schedule;
import com.amen.wrap.model.planner.SimpleTask;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Path("/")
public class SchedulesServlet {

	@POST
	@Path("/getSchedule/{id}")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getSchedule(String data) {
		JSONObject scheduleInfo = new JSONObject(data);

		int scheduleId = scheduleInfo.getInt("id");

		return Response.status(200).build();
	}

	@GET
	@Path("/getSchedules")
	@Produces("application/json")
	public Response getSchedules(String data) {
		Result schedules = ScheduleDao.getInstance().getSchedules();

		return Response.status(200).entity(composeResult(schedules).toString()).build();
	}

	@POST
	@Path("/addSchedule")
	@Consumes("application/json")
	@Produces("application/json")
	public Response addSchedule(String data) {
		JSONObject scheduleObj = new JSONObject(data);
		JSONArray tasks = scheduleObj.getJSONArray("tasks");

		Schedule schedule = new Schedule();
		schedule.setDate(LocalDate.parse(scheduleObj.getString("date"), DateTimeFormatter.ofPattern("yyyy/MM/dd")));

		for (int i = 0; i < tasks.length(); i++) {
			JSONObject singleTask = tasks.getJSONObject(i);
			SimpleTask singleTaskInstance = new SimpleTask();
			singleTaskInstance.setName(singleTask.getString("name"));
			singleTaskInstance.setSchedule(schedule);
			singleTaskInstance
					.setTime(LocalTime.parse(singleTask.getString("time"), DateTimeFormatter.ofPattern("HH:mm")));
			if (singleTask.has("location")) {
				JSONObject locationObj = singleTask.getJSONObject("location");

				double lat = locationObj.getDouble("latitude");
				double lon = locationObj.getDouble("longitude");

				Location location = new Location(lat, lon);
				singleTaskInstance.setLocation(location);
			}

			schedule.getTasks().add(singleTaskInstance);
		}

		ScheduleDao.getInstance().addSchedule(schedule);

		return Response.status(200).entity("{}").build();
	}
	
	@POST
	@Path("/getScheduleForToday")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getScheduleForToday(String data){
		Result schedules = ScheduleDao.getInstance().getScheduleFor(new SimpleDateFormat("yyyy/MM/dd").format(new Date()));

		return Response.status(200).entity(composeResult(schedules).toString()).build();
	}

	@POST
	@Path("/removeSchedule")
	@Consumes("application/json")
	@Produces("application/json")
	public Response removeSchedule(String data) {

		return Response.status(200).build();
	}

	private JSONObject composeResult(Result resultToCompose) {
		JSONObject resultObject = new JSONObject();

		resultObject.put("result", resultToCompose.getResult().toString());
		if (resultToCompose.getReturnable() instanceof List) {
			List<IJSONParsable> parsables = (List<IJSONParsable>) resultToCompose.getReturnable();

			JSONArray array = new JSONArray();
			for (IJSONParsable parsable : parsables) {
				array.put(parsable.toJSON());
			}

			resultObject.put("instance", array);
		} else if (resultToCompose.getReturnable() instanceof IJSONParsable) {
			IJSONParsable parsable = (IJSONParsable) resultToCompose.getReturnable();
			resultObject.put("returnable", parsable.toJSON());
		}

		return resultObject;
	}

}
